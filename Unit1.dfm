object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 427
  ClientWidth = 725
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 32
    Top = 32
    Width = 23
    Height = 13
    Caption = 'Add:'
  end
  object Label6: TLabel
    Left = 56
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Surname:'
  end
  object Label7: TLabel
    Left = 71
    Top = 83
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label1: TLabel
    Left = 67
    Top = 110
    Width = 35
    Height = 13
    Caption = 'Height:'
  end
  object Label3: TLabel
    Left = 41
    Top = 232
    Width = 46
    Height = 13
    Caption = 'Surname:'
  end
  object Label4: TLabel
    Left = 56
    Top = 259
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label5: TLabel
    Left = 52
    Top = 286
    Width = 35
    Height = 13
    Caption = 'Height:'
  end
  object Label8: TLabel
    Left = 17
    Top = 208
    Width = 216
    Height = 13
    Caption = 'Find mac height form 4-th human (included)::'
  end
  object SurnameFind: TLabel
    Left = 121
    Top = 232
    Width = 42
    Height = 13
    Caption = 'Surname'
  end
  object NameFind: TLabel
    Left = 121
    Top = 259
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object HeightFind: TLabel
    Left = 121
    Top = 286
    Width = 31
    Height = 13
    Caption = 'Height'
  end
  object Label12: TLabel
    Left = 400
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Surname:'
  end
  object Label13: TLabel
    Left = 415
    Top = 83
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label14: TLabel
    Left = 411
    Top = 110
    Width = 35
    Height = 13
    Caption = 'Height:'
  end
  object Label15: TLabel
    Left = 384
    Top = 32
    Width = 26
    Height = 13
    Caption = 'View:'
  end
  object SurnameShow: TLabel
    Left = 480
    Top = 56
    Width = 42
    Height = 13
    Caption = 'Surname'
  end
  object NameShow: TLabel
    Left = 480
    Top = 83
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object HeightShow: TLabel
    Left = 480
    Top = 110
    Width = 31
    Height = 13
    Caption = 'Height'
  end
  object Label16: TLabel
    Left = 223
    Top = 110
    Width = 13
    Height = 13
    Caption = 'cm'
  end
  object SurnameAdd: TEdit
    Left = 120
    Top = 53
    Width = 97
    Height = 21
    TabOrder = 0
    OnClick = SurnameAddClick
  end
  object NameAdd: TEdit
    Left = 120
    Top = 80
    Width = 97
    Height = 21
    TabOrder = 1
    OnClick = SurnameAddClick
  end
  object AddB: TButton
    Left = 80
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 2
    OnClick = AddBClick
  end
  object HeightAdd: TEdit
    Left = 120
    Top = 107
    Width = 97
    Height = 21
    TabOrder = 3
    OnClick = SurnameAddClick
  end
  object RemoveB: TButton
    Left = 480
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Remove'
    TabOrder = 4
    OnClick = RemoveBClick
  end
  object PrevB: TButton
    Left = 384
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Prev'
    TabOrder = 5
    OnClick = PrevBClick
  end
  object NextB: TButton
    Left = 576
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Next'
    TabOrder = 6
    OnClick = NextBClick
  end
  object FindB: TButton
    Left = 80
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Find'
    TabOrder = 7
    OnClick = FindBClick
  end
end
